//
//  Init.h
//  2048
//
//  Created by Александр on 16.11.14.
//  Copyright (c) 2014 Валькович Александр. All rights reserved.
//

#ifndef _048_Init_h
#define _048_Init_h

typedef struct GAMEINFO
{
    bool have_2048;
    Square matrix[4][4];
    int scores;
} GameStats;

GameStats CreateGameStats(Square matrix[4][4], int scores, bool have_2048)
{
    GameStats stats;
    memcpy(stats.matrix, matrix, sizeof(Square)*16);
    stats.scores = scores;
    stats.have_2048 = have_2048;
    return  stats;
}


#endif
