//
//  GameSquare.h
//  2048
//
//  Created by Александр on 19.10.14.
//  Copyright (c) 2014 Валькович Александр. All rights reserved.
//

#ifndef _048_GameSquare_h
#define _048_GameSquare_h


typedef struct SQUARE
{
    int value;
    float x;
    float y;
    float dx,rx;
    float dy,ry;
    int delta_value;
    bool need_refresh;
    bool start_flag;
    bool size;
    
} Square ;


#endif
