//
//  IntegerStack.h
//  ЯП(10.1)_var2
//
//  Created by Александр on 10.11.14.
//  Copyright (c) 2014 Валькович Александр. All rights reserved.
//

#ifndef _MatrixStack_h
#define _MatrixStack_h

typedef struct _Element
{
    GameStats value;
    struct _Element *next;
} Element;

typedef struct TStack
{
    Element *head;
} Stack;

void Create(Stack * stack)
{
    stack->head = NULL;
}

void Destroy(Stack * stack)
{
    while(stack->head)
    {
        Element *tmp = stack->head;
        stack->head = stack->head->next;
        free(tmp);
    }
}

int Push(Stack * stack, GameStats val)
{
    Element *tmp = (Element*)malloc(sizeof(GameStats));
    
    if(!tmp)
        return 0;
    
    if (stack->head)
    {
        tmp->next = stack->head;
        tmp->value = val;
        stack->head = tmp;
    }
    else
    {
        tmp->value = val;
        tmp->next = NULL;
        stack->head = tmp;
    }
    return 1;
}

int Pop(Stack * stack, GameStats *val)
{
    if(!stack->head)
        return 0;
    
    Element *tmp = stack->head;
    *val = stack->head->value;
    
    stack->head = stack->head->next;
    
    free(tmp);
    
    return 1;
}

void Show(Stack * stack)
{
    Stack buf;
    Create(&buf);
    
    while (stack->head)
    {
        GameStats value;
        Pop(stack, &value);
        Push(&buf,value);
    }
    
    while (buf.head)
    {
        GameStats value;
        Pop(&buf, &value);
        Push(stack,value);
    }
    
    Destroy(&buf);
    
}

#endif
