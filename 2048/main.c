

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include "GameSquare.h"
#include "Colors.h"
#include "Init.h"
#include <string.h>
#include <time.h>
#include "MatrixStack.h"
#include <unistd.h>

#if defined(__APPLE__)
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
#endif

int time_val;

char title[] = "2048";
int windowWidth  = 440;
int windowHeight = 480;
float refreshMillis = 4;
float variable = 0;
bool timer_flag = true;
bool size_flag;
float size_koef = 0;
float space_between_squares;
float one_segment_size;
float offset = 0.2f;

typedef struct VECTOR2
{
    float x;
    float y;
} Vector2;

/// Серединные точки каждого квадрата
Vector2 center_points[4][4];

/// Массив квадратов
Square squares[4][4];

int startlerp;

typedef void (*ButtonCallback)();

/// Содержимое квадратов
Square matrix[4][4];

/// Очки игрока
int scores = 0;

/// Рекорд игрока
int high_scores = 0;

/// Стек ходов
Stack stack;

/// Буф переменная для хрпанения информации о предыдущих ходах
GameStats stats;

/// Разрешение на запись ходов
bool can_save;

bool move_flag = false;

/// Переменная запоминающая состояние победы
bool have_2048 = false;

/// Отрисовка победы
bool victory_flag = false;
/// Отрисовка неудачи
bool failure_flag = false;

/// Переманная, хранящая данные о необходимости добавить новую клетку на
/// экран в конце цикла.
bool need_random  = false;

Vector2 bottom_left = {-0.7f, -0.7f};
Vector2 up_left = {-0.7f, 0.5};
Vector2 up_right = {0.7f, 0.5f};
Vector2 bottom_right = {0.7f, -0.7f};

typedef struct Button
{
    int   x;
    int   y;
    int   w;
    int   h;
    ButtonCallback callbackFunction;
} Button;


void Back();
void NewGame();
Button UndoButton = {310,80, 80,40, Back };
Button NewGameButton = {55,80, 80,40, NewGame };


///// РЕАЛИЗАЦИЯ

void PushToStats();
void GameStatusCheck();
void initGL();
void RenderBitmapString(float,float,float, void *,char *);
void SaveHighScores();
void UpdateHighScores();
void AddScores(int);
void Lerp ();
void StopSizeTimer();
void ResizeSquare();
void CreateRandom();
void ToUp();
void ToDown();
void ToRight();
void ToLeft();
void Update();
void reshape(GLsizei,GLsizei);
void Timer(int);
void LerpTimer(int);
void SaveGame();
void LoadGame();
void Keyboard(unsigned char,int,int);
void GameStatusCheck();
void Init();
void PushToStats();
int GetHighScores();
int ButtonClick(Button*,float,float);
void MouseButton(int,int,int,int);

void initGL()
{
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(1, 0.996f, 0.937f, 1);
}

void RenderBitmapString(float x, float y,float z, void *font,char *string)
{
    char *c;
    glRasterPos3f(x,y,z);
    for (c=string; *c != '\0'; c++)
        glutBitmapCharacter(font, *c);
}

void SaveHighScores()
{
    if (scores < high_scores || scores == 0)
        return;
    
    char user[50];
    strcpy(user,getlogin());
    
    char path_scores[100];
    strcpy(path_scores, "/Users/");
    strcat(path_scores, user);
    strcat(path_scores,"/scores.dat");
    
    FILE * scores_file = fopen(path_scores, "wb");
    fwrite(&high_scores, sizeof(int), 1, scores_file);
    fclose(scores_file);
}

void UpdateHighScores()
{
      high_scores = scores;
}

void AddScores(int new_scores)
{
    scores += new_scores;
    if (scores > high_scores)
        UpdateHighScores();
}

void Lerp ()
{
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            if ((matrix[i][j].dx != matrix[i][j].x) && matrix[i][j].dx != 0)
            {
                    matrix[i][j].x -=  matrix[i][j].rx;
                    matrix[i][j].dx +=  matrix[i][j].rx;
            }
            
            if ((matrix[i][j].dy != matrix[i][j].y) && matrix[i][j].dy != 0)
            {
                matrix[i][j].y -=  matrix[i][j].ry;
                matrix[i][j].dy +=  matrix[i][j].ry;
            }
        }

       }

void StopSizeTimer()
{
    size_flag = false;
    
    for (int i = 0; i < 4; i++)
        for (int j = 0; j< 4; j++)
        {
            matrix[i][j].start_flag = false;
        }


    GameStatusCheck();  /// Проверим на завершение игры
    size_koef = 0;
    timer_flag = true;
}

void ResizeSquare()
{
    size_koef += 0.02;
}

void CreateRandom()
{
    int i = rand() % 4;
    int j = rand() % 4;
    
 //   printf("CРАБОТАЛО\n");
    
    if (matrix[i][j].value == 0)
    {
        srand((unsigned)time(NULL));
        int chance = rand() % 9;
        
        if (chance == 8) matrix[i][j].value = 4;
        else matrix[i][j].value = 2;
        
        matrix[i][j].start_flag = true;
        matrix[i][j].need_refresh = false;
        
        size_flag = true;
        glutTimerFunc(100, StopSizeTimer, 0);
        
    }
    else CreateRandom();
}

void ToUp()
{
      
    if (!timer_flag)
        return;
    
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 3; i++)
            {
                int index = i + 1;
                for (; index < 3; index++)
                {
                    if (matrix[index][j].value != 0)
                        break;
                }
                
                if ((matrix[i][j].value == matrix[index][j].value) && matrix[i][j].value > 0)
                {
                    matrix[i][j].value += matrix[index][j].value;
                    AddScores(matrix[index][j].value);
                    
                    matrix[index][j].value = 0;
                    
                    
                    matrix[index][j].dy = matrix[i][j].y;
                    matrix[index][j].ry = (matrix[index][j].y - matrix[index][j].dy) / 15;
                    
                    matrix[index][j].need_refresh = true;
                    i += 1;
                   // j = 0;
                }
            }
        
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 4; j++)
                if (matrix[i][j].value == 0)
                {
                    int index;
                    for (index = i+ 1; index < 3 ; index++)
                    {
                        if (matrix[index][j].value != 0)
                            break;
                    }
                    
                    matrix[i][j].value += matrix[index][j].value;
                    matrix[index][j].value = 0;
                    
                    if (matrix[index][j].dy == 0)
                    {
                        matrix[index][j].dy = matrix[i][j].y;
                        matrix[index][j].ry = (matrix[index][j].y - matrix[index][j].dy) / 15;
                    }
                    
                    matrix[i][j].need_refresh = true;
                }
        
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
            {
                if ((matrix[i][j].value == 0) && (matrix[i][j].delta_value == 0))
                {
                    matrix[i][j].dx = 0;
                    matrix[i][j].dy = 0;
                    matrix[i][j].rx = 0;
                }
            }

}

void ToDown()
{
    if (!timer_flag)
        return;
    
    for (int j = 0; j < 4; j++)
        for (int i = 3; i > 0; i--)
            {
                int index = i - 1;
                for (; index > 0; index--)
                {
                    if (matrix[index][j].value != 0)
                        break;
                }
                
                if ((matrix[i][j].value == matrix[index][j].value) && matrix[i][j].value > 0)
                {
                    matrix[i][j].value += matrix[index][j].value;
                    AddScores(matrix[index][j].value);
                    matrix[index][j].value = 0;
                    
                    matrix[index][j].dy = matrix[i][j].y;
                    matrix[index][j].ry = (matrix[index][j].y - matrix[index][j].dy) / 15;
                    
                    matrix[index][j].need_refresh = true;
                    i -= 1;
                  //  j = 0;
                }
            }
    
        for (int i = 3; i > 0; i--)
            for (int j = 0; j < 4; j++)
                if (matrix[i][j].value == 0)
                {
                    int index;
                    for (index = i - 1; index > 0 ; index--)
                    {
                        if (matrix[index][j].value != 0)
                            break;
                    }
                    
                    matrix[i][j].value += matrix[index][j].value;
                    matrix[index][j].value = 0;
                    
                    if (matrix[index][j].dy == 0)
                    {
                        matrix[index][j].dy = matrix[i][j].y;
                        matrix[index][j].ry = (matrix[index][j].y - matrix[index][j].dy) / 15;
                    }
                    
                    
                    matrix[i][j].need_refresh = true;
                }
    
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
            {
                if ((matrix[i][j].value == 0) && (matrix[i][j].delta_value == 0))
                {
                    matrix[i][j].dx = 0;
                    matrix[i][j].dy = 0;
                    matrix[i][j].rx = 0;
                }
            }
}

void ToLeft()
{
    if (!timer_flag)
        return;
    
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 3; j++)
            {
                int index = j + 1;
                for (; index < 3; index++)
                {
                    if (matrix[i][index].value != 0)
                        break;
                }
                
                if ((matrix[i][j].value == matrix[i][index].value) && matrix[i][j].value > 0)
                {
                    matrix[i][j].value += matrix[i][index].value;
                    AddScores(matrix[i][index].value);
                    matrix[i][index].value = 0;
                    
                    matrix[i][index].dx = matrix[i][j].x;
                    matrix[i][index].rx = (matrix[i][index].x - matrix[i][index].dx) / 15;
                    
                    matrix[i][index].need_refresh = true;
                    j += 1;
 //                   i = 0;
                }
            }
        
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 3; j++)
                if (matrix[i][j].value == 0)
                {
                    int index;
                    for (index = j+ 1; index < 3 ; index++)
                    {
                        if (matrix[i][index].value != 0)
                            break;
                    }
                    
                    matrix[i][j].value += matrix[i][index].value;
                    matrix[i][index].value = 0;
                    
                    if (matrix[i][index].dx == 0)
                    {
                        matrix[i][index].dx = matrix[i][j].x;
                        matrix[i][index].rx = (matrix[i][index].x - matrix[i][index].dx) / 15;
                    }
                    
                    
                    matrix[i][j].need_refresh = true;
                }
        
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
            {
                if ((matrix[i][j].value == 0) && (matrix[i][j].delta_value == 0))
                {
                    matrix[i][j].dx = 0;
                    matrix[i][j].dy = 0;
                    matrix[i][j].rx = 0;
                }
            }

}

void ToRight()
{
    if (!timer_flag)
        return;
    
    for (int i = 0; i < 4; i++)
        for (int j = 3; j > 0; j--)
        {
            int index = j-1;
            for (; index > 0; index--)
            {
               if (matrix[i][index].value != 0)
                   break;
            }
            
            if ((matrix[i][j].value == matrix[i][index].value) && matrix[i][j].value > 0)
            {
                matrix[i][j].value += matrix[i][index].value;
                AddScores(matrix[i][index].value);
                matrix[i][index].value = 0;
                
                matrix[i][index].dx = matrix[i][index].x;
                matrix[i][index].rx = (matrix[i][index].x - matrix[i][index].dx) / 15;
                
                matrix[i][index].need_refresh = true;
                j -= 1;
               // i = 0;
            }
        }
    
    for (int i = 0; i < 4; i++)
        for (int j = 3; j > 0; j--)
            if (matrix[i][j].value == 0)
            {
                int index;
                for (index = j-1; index > 0 ; index--)
                {
                    if (matrix[i][index].value != 0)
                        break;
                }
            
                matrix[i][j].value += matrix[i][index].value;
                matrix[i][index].value = 0;
                
                if (matrix[i][index].dx == 0)
                {
                    matrix[i][index].dx = matrix[i][j].x;
                    matrix[i][index].rx = (matrix[i][index].x - matrix[i][index].dx) / 15;
                }

     
                matrix[i][j].need_refresh = true;
            }
     
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            if ((matrix[i][j].value == 0) && (matrix[i][j].delta_value == 0))
            {
                matrix[i][j].dx = 0;
                matrix[i][j].dy = 0;
                matrix[i][j].rx = 0;
            }
        }
}

void Update()
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /// Лучшие очки - обводка
    glBegin(GL_QUADS);
        glColor3f(0.8f, 0.76f, 0.705f);
        glVertex2f(up_right.x - 0.4f, 1 - (1 - up_right.y - offset)+ offset);
        glVertex2f(up_right.x - 0.4f, 1 - (1 - up_right.y - offset));
        glVertex2f(up_right.x,1 - (1 - up_right.y - offset));
        glVertex2f(up_right.x, 1 - (1 - up_right.y - offset)+ offset);
    glEnd();
    
    /// Текущие очки - обводка
    glBegin(GL_QUADS);
        glColor3f(0.8f, 0.76f, 0.705f);
        glVertex2f(up_right.x - 0.85f, 1 - (1 - up_right.y - offset)+ offset);
        glVertex2f(up_right.x - 0.85f, 1 - (1 - up_right.y - offset));
        glVertex2f(up_right.x-0.45f,1 - (1 - up_right.y - offset));
        glVertex2f(up_right.x-0.45f, 1 - (1 - up_right.y - offset)+ offset);
    glEnd();
    
    /// Откат - обводка
    glBegin(GL_QUADS);
    glColor3f(0.8f, 0.76f, 0.705f);
    glVertex2f(up_right.x - 0.3f, 1 - (1 - up_right.y - offset) - 0.17f);
    glVertex2f(up_right.x - 0.3f, 1 - (1 - up_right.y - offset) - 0.03f);
    glVertex2f(up_right.x,1 - (1 - up_right.y - offset) - 0.03f);
    glVertex2f(up_right.x, 1 - (1 - up_right.y - offset) - 0.17f);
    glEnd();
    
    /// Новая игра - обводка
    glBegin(GL_QUADS);
    glColor3f(0.8f, 0.76f, 0.705f);
    glVertex2f(up_left.x + 0.3f, 1 - (1 - up_left.y - offset) - 0.17f);
    glVertex2f(up_left.x + 0.3f, 1 - (1 - up_left.y - offset) - 0.03f);
    glVertex2f(up_left.x,1 - (1 - up_left.y - offset) - 0.03f);
    glVertex2f(up_left.x, 1 - (1 - up_left.y - offset) - 0.17f);
    glEnd();
    
    /// Поле
    glBegin(GL_QUADS);
        glColor3f(0.8f, 0.76f, 0.705f);
        glVertex2f(bottom_left.x, bottom_left.x - offset);
        glVertex2f(up_left.x, up_left.y);
        glVertex2f(up_right.x , up_right.y);
        glVertex2f(bottom_right.x, bottom_right.y - offset);
    glEnd();
    


    /// Пустые квадраты
    float current_x, current_y;
    current_x = current_y = 0;
    
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j< 4; j++)
        {
            glBegin(GL_POLYGON);
            glColor3f(0.73f, 0.68f, 0.63f);
            glVertex2f(center_points[i][j].x - one_segment_size / 2, center_points[i][j].y + one_segment_size / 2);
            glVertex2f(center_points[i][j].x + one_segment_size / 2, center_points[i][j].y + one_segment_size / 2);
            glVertex2f(center_points[i][j].x + one_segment_size / 2, center_points[i][j].y - one_segment_size / 2);
            glVertex2f(center_points[i][j].x - one_segment_size / 2, center_points[i][j].y - one_segment_size / 2);
            glEnd();
       
        }
        
    }
    
    /// Не пустые квадраты
    
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j< 4; j++)
        {
            if ((matrix[i][j].dx > 0.0f || matrix[i][j].dy > 0.0f || matrix[i][j].dx < 0.0f || matrix[i][j].dy < 0.0f) && matrix[i][j].delta_value > 0  )
            {
                glBegin(GL_QUADS);
                    Color color = GetColor(matrix[i][j].delta_value);
                    glColor3f(color.r, color.g , color.b);
                    glVertex2f(matrix[i][j].x - one_segment_size / 2, matrix[i][j].y  - one_segment_size / 2);
                    glVertex2f(matrix[i][j].x - one_segment_size / 2, matrix[i][j].y + one_segment_size / 2);
                    glVertex2f(matrix[i][j].x + one_segment_size / 2, matrix[i][j].y + one_segment_size / 2);
                    glVertex2f(matrix[i][j].x + one_segment_size / 2, matrix[i][j].y - one_segment_size / 2);
                glEnd();
            
                color = GetTextColor(matrix[i][j].delta_value);
                glColor3f(color.r, color.g , color.b);
                char c[20];
                sprintf(c, "%d", matrix[i][j].delta_value);
            
                if (matrix[i][j].value > 0)
                    RenderBitmapString(center_points[i][j].x-(0.02f*strlen(c)),center_points[i][j].y-0.02f,-1,GLUT_BITMAP_HELVETICA_18,c);
            }
            else if (matrix[i][j].value > 0 && !matrix[i][j].need_refresh)
            {
                
                glBegin(GL_QUADS);
                    Color color = GetColor(matrix[i][j].value);
                    glColor3f(color.r, color.g , color.b);
                
                    if (matrix[i][j].start_flag)
                    {
                        glVertex2f(matrix[i][j].x - (one_segment_size / 2 * size_koef), matrix[i][j].y  - (one_segment_size / 2 * size_koef));
                        glVertex2f(matrix[i][j].x - (one_segment_size / 2 * size_koef), matrix[i][j].y + (one_segment_size / 2 * size_koef));
                        glVertex2f(matrix[i][j].x + (one_segment_size / 2 * size_koef), matrix[i][j].y + (one_segment_size / 2 * size_koef));
                        glVertex2f(matrix[i][j].x + (one_segment_size / 2 * size_koef), matrix[i][j].y - (one_segment_size / 2 * size_koef));
                    }
                    else
                    {
                        glVertex2f(matrix[i][j].x - one_segment_size / 2, matrix[i][j].y  - one_segment_size / 2);
                        glVertex2f(matrix[i][j].x - one_segment_size / 2, matrix[i][j].y + one_segment_size / 2);
                        glVertex2f(matrix[i][j].x + one_segment_size / 2, matrix[i][j].y + one_segment_size / 2);
                        glVertex2f(matrix[i][j].x + one_segment_size / 2, matrix[i][j].y - one_segment_size / 2);
                    }
                glEnd();
                
                color = GetTextColor(matrix[i][j].value);
                glColor3f(color.r, color.g, color.b);
                char c[20];
                sprintf(c, "%d", matrix[i][j].value);
                
                if (matrix[i][j].value > 0)
                    RenderBitmapString(center_points[i][j].x-(0.02f*strlen(c)),center_points[i][j].y-0.02f,-1,GLUT_BITMAP_HELVETICA_18,c);
            }
        }
    }

    glColor3f(0.466f, 0.431f, 0.396f);
        RenderBitmapString(-0.54f,0.82f,-1,GLUT_BITMAP_HELVETICA_18,"2048");
        RenderBitmapString(-0.62f,0.75f,-1,GLUT_BITMAP_HELVETICA_12,"Classic mode");
        RenderBitmapString(-0.35f,0.6f,-1,GLUT_BITMAP_HELVETICA_12,"Join the numbers and get to");
        RenderBitmapString(-0.35f,0.54f,-1,GLUT_BITMAP_HELVETICA_12,"the 2048 tile!");
        RenderBitmapString(bottom_right.x - 0.3f,bottom_right.y-0.25,-1,GLUT_BITMAP_HELVETICA_10,"valkovich.com");;
    glColor3f(0.933f, 0.894f, 0.854f);
        RenderBitmapString((up_right.x - 0.85f) + 0.15f,1 - (1 - up_right.y - offset)+0.14f,-1,GLUT_BITMAP_HELVETICA_10,"Score");
        RenderBitmapString((up_right.x - 0.4f) + 0.15f,1 - (1 - up_right.y - offset)+0.14f,-1,GLUT_BITMAP_HELVETICA_10,"Best");
        RenderBitmapString((up_right.x - 0.4f) + 0.2f,1 - (1 - up_right.y - offset)-0.11f,-1,GLUT_BITMAP_HELVETICA_10,"Undo");
        RenderBitmapString((up_left.x + 0.1f),1 - (1 - up_right.y - offset)-0.11f,-1,GLUT_BITMAP_HELVETICA_10,"New");

    
    char char_scores[20];
    sprintf(char_scores, "%d", scores);
    RenderBitmapString(((up_right.x - 0.81f) + 0.17f)-(0.02f*strlen(char_scores)),1 - (1 - up_right.y - offset) + 0.05f,-1,GLUT_BITMAP_HELVETICA_18,char_scores);
    
    sprintf(char_scores, "%d", high_scores);
    RenderBitmapString(((up_right.x - 0.81f) + 0.61f)-(0.02f*strlen(char_scores)),1 - (1 - up_right.y - offset) + 0.05f,-1,GLUT_BITMAP_HELVETICA_18,char_scores);
    

    if (failure_flag)
    {
        glBegin(GL_QUADS);
            glColor4f(1, 0.996f, 0.937f, 0.8f);
            glVertex2f(bottom_left.x, bottom_left.x - offset);
            glVertex2f(up_left.x, up_left.y);
            glVertex2f(up_right.x , up_right.y);
            glVertex2f(bottom_right.x, bottom_right.y - offset);
        glEnd();
        
        glColor3f(0.466f, 0.431f, 0.396f);
        RenderBitmapString(-0.2f,-0.15f,-1,GLUT_BITMAP_HELVETICA_18,"YOU LOSE");
        RenderBitmapString(-0.5f,-0.3f,-1,GLUT_BITMAP_HELVETICA_12,"Press UNDO or NEW GAME to try again");
    }

    
    if (victory_flag)
    {
        glBegin(GL_QUADS);
            glColor4f(1, 0.996f, 0.937f, 0.8f);
            glVertex2f(bottom_left.x, bottom_left.x - offset);
            glVertex2f(up_left.x, up_left.y);
            glVertex2f(up_right.x , up_right.y);
            glVertex2f(bottom_right.x, bottom_right.y - offset);
        glEnd();
        
        glColor3f(0.466f, 0.431f, 0.396f);
            RenderBitmapString(-0.2f,-0.15f,-1,GLUT_BITMAP_HELVETICA_18,"YOU WIN");
            RenderBitmapString(-0.6f,-0.3f,-1,GLUT_BITMAP_HELVETICA_18,"Press any key to continue game");
    }
    
       glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height)
{
    glutReshapeWindow(windowWidth, windowHeight);
}

void Timer(int value)
{
    glutPostRedisplay();    // Post a paint request to activate display()
    time_val++;
   
    if (!timer_flag)
        Lerp();
    
    if (size_flag)
        ResizeSquare();
  
    
    glutTimerFunc(refreshMillis, Timer, 0); // subsequent timer call at milliseconds
}

void LerpTimer(int value)
{
    time_val = 0;
    
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            matrix[i][j].dx  =  0;
            matrix[i][j].dy  =  0;
            matrix[i][j].rx = 0;
            matrix[i][j].ry = 0;
              matrix[i][j].x = center_points[i][j].x;
              matrix[i][j].y = center_points[i][j].y;
            
            if (matrix[i][j].value > 0)
            {
                matrix[i][j].need_refresh = false;
                matrix[i][j].delta_value = 0;
            }
        }
    
    if (need_random)
    {
        glutTimerFunc(210, CreateRandom, 0);
        need_random = false;
    }
}

void SaveGame()
{
    PushToStats();
    
    for (unsigned i =0; i < 4; i++)
    {
        for (unsigned j =0; j < 4; j++)
        {
            printf("%d ",stats.matrix[i][j].value);
        }
        
        printf("\n");
    }
    
    char user[50];
    strcpy(user,getlogin());
    
    char path_scores[100];
    strcpy(path_scores, "/Users/");
    strcat(path_scores, user);
    strcat(path_scores,"/2048game.dat");
    
    FILE * savegame_file = fopen(path_scores, "wb");
    
    if (!savegame_file)
    {
        printf("Файл не создан. Отказано в доступе. \n");
        return;
    }
    
    while(Pop(&stack, &stats))
        fwrite(&stats, sizeof(Element), 1, savegame_file);
    
    fclose(savegame_file);
}

void LoadGame()
{
    char user[50];
    strcpy(user,getlogin());
    
    char path_scores[100];
    strcpy(path_scores, "/Users/");
    strcat(path_scores, user);
    strcat(path_scores,"/2048game.dat");
    
    FILE * savegame_file = fopen(path_scores, "rb");
    
    if (!savegame_file)
    {
        printf("Файл сохранений не открыт. \n");
        return;
    }
    
    Stack bufStack;
    
    while (true)
    {
        fread(&stats, sizeof(Element), 1, savegame_file);
        if (feof(savegame_file))
            break;
        
        Push(&bufStack, stats);
    }
    
    if (!bufStack.head)
    {
        printf("Стек возврата пуст. \n");
        fclose(savegame_file);
        return;
    }
    
    while (Pop(&bufStack, &stats))
    {
        Push(&stack, stats);
    }
    
    printf("Загружено\n");
    
    memcpy(matrix, stats.matrix, sizeof(Square)*16);
    have_2048 = stats.have_2048;
    scores = stats.scores;
    
    Back();
    can_save = true;
    
    fclose(savegame_file);
}

void Keyboard(unsigned char key, int x, int y)
{
    printf("%d",(int)key);
    if (!victory_flag && !failure_flag)
    {
    
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            matrix[i][j].delta_value = matrix[i][j].value;
        }
    
    switch (key)
    {
        case 27:
            SaveGame();
            SaveHighScores();
            exit(0);break;
            
        case 100    : // d
        case 68     : // D
            PushToStats();
            ToRight(); break;
            
        case 119    : // w
        case 87     : // W
                PushToStats();
            ToUp(); break;
            
        case 115    : // s
        case 83     : // S
                PushToStats();
            ToDown(); break;
            
        case 97     : // a
        case 65     : // A
                PushToStats();
            ToLeft(); break;
            
        case 122    : // Back
            Back(); return;
            
        default:
            break;
    }
    
    bool check_for_new_random = true;
    
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            if (matrix[i][j].value != matrix[i][j].delta_value)
            {
                check_for_new_random = false;
                break;
            }
        }
    
    if (!check_for_new_random)
        need_random = true;
        
    
    if (need_random)
    {
        timer_flag = false;
        startlerp = time_val;
        glutTimerFunc(100, LerpTimer, 0); // Скорость игры
    }
    else GameStatusCheck();  /// Проверим на завершение игры

    }
    else
    {
        if (victory_flag)
            victory_flag = false;
        if (failure_flag)
            if ((int)key == 27)
            {
                SaveGame();
                SaveHighScores();
                exit(0);
            }
    }
}

int CheckClamp(int value)
{
    if (value < 0)
        return 0;
    else if (value > 3) return 3;
    else return value;
}

void GameStatusCheck()
{
    printf("Запуск проверки \n");
    bool free_square = false;
    
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            if (matrix[i][j].value == 0)
            {
                free_square = true;
                break;
            }
        }
    
    if (!free_square)
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                    if ((matrix[i][j].value == matrix[i+1][j].value) ||
                        (matrix[i][j].value == matrix[i][j+1].value) ||
                        (matrix[3][j].value == matrix[3][j+1].value) ||
                        (matrix[i][3].value == matrix[i+1][3].value) )
                        free_square = true;
            }
    
    
    if (!free_square)
    {
        failure_flag = true;
        printf("Игрок продул \n");
        return;
    }
    
    if (!have_2048)
    {
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
            {
                if (matrix[i][j].value == 2048)
                {
                    victory_flag = true;
                    have_2048 = true;
                    printf("Победа \n");
                }
            }
    }
}

void Init()
{
    float full_space = fabsf(up_left.x) + fabsf(up_right.x);
    
    // 30 - методом подбора
    one_segment_size = ((full_space - full_space / 30) / 4);
    space_between_squares = one_segment_size / 8;
    one_segment_size -= space_between_squares;
    
    float free_space_x, free_space_y = space_between_squares;
    
    for (int i = 0; i < 4; i++)
    {
        free_space_x = space_between_squares;
        
        for (int j = 0; j < 4; j++)
        {
            center_points[i][j].x =  up_left.x  + free_space_x + (one_segment_size ) * (j+1) - (one_segment_size/2);
            center_points[i][j].y =  up_left.y  - free_space_y - (one_segment_size) * (i+1) + (one_segment_size/2) ;
            free_space_x += space_between_squares;
        }
        
        free_space_y += space_between_squares;
    }
    
    /// Забъем матрицу значений
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            matrix[i][j].value = 0;
    
    CreateRandom();
    CreateRandom();

    
//        matrix[0][0].value = 4;
//        matrix[0][1].value = 4;
//        matrix[0][2].value = 8;
//        matrix[0][3].value = 16;
//    
//    matrix[1][0].value = 256;
//    matrix[1][1].value = 128;
//    matrix[1][2].value = 64;
//    matrix[1][3].value = 32;
//    
//    matrix[2][0].value = 512;
//    matrix[2][1].value = 1024;
//    matrix[2][2].value = 2048;
//    matrix[2][3].value = 4096;
//    
//    matrix[3][3].value = 8192;
//    matrix[3][2].value = 16384;
//    matrix[3][1].value = 32768;
//    matrix[3][0].value = 65536;

    
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            matrix[i][j].x = center_points[i][j].x;
            matrix[i][j].y = center_points[i][j].y;
        }
}

void PushToStats()
{
    can_save = true;
    stats = CreateGameStats(matrix,scores,have_2048);
   
    Push(&stack, stats);    // Записываем данные успешного хода
    printf("Запушили последний ход \n");
}

void Back()
{
    if (!can_save)
        return;
    
    failure_flag = false;
    victory_flag = false;
    
    if (!stack.head)
    {
        printf("Откат невозможен \n");
        return;
    }
    
    Pop(&stack, &stats);
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
        {
            matrix[i][j].value = stats.matrix[i][j].value;
            matrix[i][j].need_refresh = false;
        }
    
    have_2048 = stats.have_2048;
    scores = stats.scores;
    
    printf("Откатились на 1 ход назад \n");
}

int GetHighScores()
{
    high_scores = scores;
    
    char user[50];
    strcpy(user,getlogin());
    
    char path_scores[100];
    strcpy(path_scores, "/Users/");
    strcat(path_scores, user);
    strcat(path_scores,"/scores.dat");
    
    FILE * scores_file = fopen(path_scores, "rb");
    if (!scores_file)
        return 0;
    
    int result;
    fread(&result, sizeof(int), 1, scores_file);
    fclose(scores_file);
    return result;
}

void NewGame()
{
    SaveHighScores();
    
    scores = 0;
    failure_flag = false;
    victory_flag = false;
    high_scores = GetHighScores();
    while (Pop(&stack, &stats))
        {}
    can_save = false;
    Init();
}

int ButtonClick(Button* b,float x,float y)
{
    if(b)
    {
        if( x > b->x && x < b->x+b->w && y > b->y && y < b->y+b->h )
        {
            b->callbackFunction();
            return 1;
        }
    }
    return 0;
}

void MouseButton(int button,int state,int x, int y)
{
    printf("%d %d %d\n", button, x,y);
    
    if (state == GLUT_DOWN)
    {
        ButtonClick(&UndoButton,x,y);
        ButtonClick(&NewGameButton,x,y);
    }
}

void SpecialInput(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_DOWN:
            Keyboard(115,0,0);
            break;
        case GLUT_KEY_UP:
            Keyboard(119,0,0);
            break;
        case GLUT_KEY_LEFT:
            Keyboard(97,0,0);
            break;
        case GLUT_KEY_RIGHT:
            Keyboard(100,0,0);
            break;
    }
}


int main(int argc, char** argv)
{
    InitColors();
    NewGame();
    LoadGame();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB|GLUT_DEPTH|GLUT_DOUBLE);
    glutInitWindowSize(windowWidth, windowHeight);
    glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH)-windowWidth)/2,(glutGet(GLUT_SCREEN_HEIGHT)-windowHeight)/2);
    glutCreateWindow(title);
    glutKeyboardFunc(Keyboard);
    glutSpecialFunc(SpecialInput);
    glutDisplayFunc(Update);
    glutReshapeFunc(reshape);
    glutTimerFunc(0, Timer, 0);
    initGL();
    glutMouseFunc(MouseButton);
    glutMainLoop();
    return 0;
}
